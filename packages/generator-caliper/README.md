# generator-caliper

A [Yeoman](http://yeoman.io) generator for scaffolding Hyperledger Caliper resources. Our generator currently generates:
* Benchmark files: the benchmark configuration and callback files used to perform benchmarks.

## Getting started
* Install: npm install -g @themedium/generator-caliper
* Run: yo @themedium/caliper

See the [documentation](https://hyperledger.github.io/caliper/vNext/benchmark-generator) for more information.